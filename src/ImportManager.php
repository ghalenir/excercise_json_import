<?php

namespace Drupal\excercise_json_import;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;

/**
 * Class ImportManager.
 */
class ImportManager {

  /**
   * Constructs a new ImportManager object.
   */
  public function __construct() {

  }

  /**
   * Import Users
   * @param $item string
   */
  public function importUsers($item) {
    $user = \Drupal\user\Entity\User::create();

    // Same password same as username
    $user->setPassword($item->username);
    $user->enforceIsNew();
    $user->setEmail($item->email);
    $user->setUsername($item->username);
    $user->set('field_address', [
      'country_code' => 'US',
      'address_line1' => $item->address->street,
      'address_line2' => $item->address->suite,
      'locality' => $item->address->city,
      'postal_code' => $item->address->zipcode,
    ]);
    $user->set('field_full_name', $item->name);
    $user->set('field_phone', $item->phone);
    $user->set('field_website', $item->website);
    $user->set('field_company', $item->company->name);
    $user->activate();
    try {
      $result = $user->save();
      return $user->id();    
    } catch (Exception $e) {
      \Drupal::logger('excercise_json_import')->error("Unable to save user with username: ".$item->email);   
      return;
    }
  }

  /**
   * Import posts
   * @param $item string
   */
  public function importPosts($item) {
    //Load node by title
    $existing_node = \Drupal::entityTypeManager()
    ->getStorage('node')
    ->loadByProperties(['title' => $item->title]);

    // Check if that node exist
    if(empty($existing_node)) {
      $node = Node::create([
        'type' => 'posts', 
        'title' => $item->title,
        'body' => [
          'value' => $item->body,
          'format' => 'full_html'
        ]
        ]);
      // Save the node
      try {
        $node->save();
        return $node->id();
      } catch(Exception $e) {
        \Drupal::logger('excercise_json_import')->error("Unable to save Node with Title: ".$item->title);   
        return;
      }
    } else {
      return;
    }
  }
}
