<?php

namespace Drupal\excercise_json_import\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class PostsImportForm.
 */
class PostsImportForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'posts_import_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = \Drupal::config('posts_import_form.settings');
    $form['json_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Json Url'),
      '#description' => $this->t('This URL will be used to import content.'),
      '#default_value' => empty($config->get('json_url')) ? "" : $config->get('json_url'),
      '#required' => true,
      '#maxlength' => 50,
      '#size' => 50,
      '#weight' => '0',
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Start Importing'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = \Drupal::service('config.factory')->getEditable('posts_import_form.settings');

    // Gather our form value.
    $json_url = $form_state->getValues()['json_url'];

    // Set and save new message value.
    $config->set('json_url', $json_url)->save();

    $batch = [];
    $batch = $this->importPostsData($json_url);
    batch_set($batch);
  }

 /**
   * Import Posts.
   * @param $json_url string
   * This process will process 1 posts at a time.
   *
   */
  public function importPostsData($json_url) {
    if(empty($json_url))
      return;
    
    // Parse the json data from url
    $post_data = json_decode(file_get_contents($json_url));
    $num_operations = 100;
    $this->messenger()->addMessage($this->t('Creating an array of @num operations', ['@num' => $num_operations]));
    $operations = [];
    $i = 0;
    foreach($post_data as $item) {
      $importer = \Drupal::service('excercise_json_import.importer')->importPosts($item);
      $operations[] = [
        'excercise_json_import_op_1',
        [
          $i + 1,
          $this->t('(Operation @operation)', ['@operation' => $i]),
        ],
      ];
      $i++;
    }
    //prepare batch
    $batch = [
      'title' => $this->t('Creating @num from this operations', ['@num' => $num_operations]),
      'operations' => $operations,
      'finished' => 'excercise_json_import_finished',
    ];
    return $batch;
  }
}
