<?php

namespace Drupal\excercise_json_import\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
/**
 * Class UserImportForm.
 */
class UserImportForm extends FormBase {

  /**s
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'user_import_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Load user config
    $config = \Drupal::config('user_import_form.settings');
    $form['user_json_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Json Url'),
      '#description' => $this->t('This URL will be used to import Users.'),
      '#default_value' => empty($config->get('user_json_url')) ? "" : $config->get('user_json_url'),
      '#required' => true,
      '#maxlength' => 50,
      '#size' => 50,
      '#weight' => '0',
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Start Importing'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = \Drupal::service('config.factory')->getEditable('user_import_form.settings');
    // Gather our form value.
    $user_json_url = $form_state->getValues()['user_json_url'];
    // Set and save new message value.
    $config->set('user_json_url', $user_json_url)->save();
    $batch = [];
    $batch = $this->importUserData($user_json_url);
    batch_set($batch);
  }

 /**
   * Import Users.
   * @param $user_json_url string
   */
  public function importUserData($user_json_url) {
    $user_data = json_decode(file_get_contents($user_json_url));
    $num_operations = 10;
    $this->messenger()->addMessage($this->t('Creating user of @num operations', ['@num' => $num_operations]));
    $operations = [];
    $i = 0;
    foreach($user_data as $user) {
      // check if the username already exist
      $existing_user = user_load_by_name($user->username);
      if(!$existing_user) {
        // try to import the user
        \Drupal::service('excercise_json_import.importer')->importUsers($user);
        $operations[] = [
          'excercise_json_import_op_1',
          [
            $i + 1,
            $this->t('(Operation @operation)', ['@operation' => $i]),
          ],
        ];
        $i++;  
      }
    }
    // prepare batch
    $batch = [
      'title' => $this->t('Creating  @num users from this operations', ['@num' => $num_operations]),
      'operations' => $operations,
      'finished' => 'excercise_json_import_finished',
    ];
    return $batch;
  }
}
