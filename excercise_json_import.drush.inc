<?php
use Drupal\taxonomy\Entity\Term;
use Drupal\user\Entity\User;

/**
 * @file Contains the code to define custom drush commands.
 */

/**
 * Implements hook_drush_command().
 */

function excercise_json_import_drush_command(){
  $commands = array();
  $commands['import-posts'] = [
    'description' => 'Drush command to import posts.',
    'aliases' => ['import:posts'],
      'options' => [
        'json_url' => 'Enter json url endpoint.',
        ]
  ];
  $commands['import-users'] = [
    'description' => 'Drush command to import users.',
    'aliases' => ['import:users'],
      'options' => [
        'json_url' => 'Enter json url endpoint.',
        ]
  ];  
  return $commands;
}

function drush_excercise_json_import_import_users() {
  // Get the value from command line
  $json_url = drush_get_option('json_url', 'N/A');
  if ($json_url == "N/A") {
    print("\n Please provide the json_url value eg. --json_url=http://example.com/json \n\n");
    return;
  }
  $user_data = json_decode(file_get_contents($json_url));
  
  $i = 1;
  foreach($user_data as $user) {
    $existing_user = user_load_by_name($user->username);
    if(!$existing_user) {
      $users_import_result = \Drupal::service('excercise_json_import.importer')->importUsers($user);
      if(!$users_import_result) {
        $message = "\n$i. User: ".$user->name." skipped.\n";
      } else {
        $message = "\n$i. User: ".$item->name." created with Uid: ".$users_import_result."\n";
      }
    } else {
      $message = "\n$i. User: ".$user->name." already exist.\n";
    }
    print_r($message);
    $i++;
  }
}

function drush_excercise_json_import_import_posts() {
  // Get the value from command line
  $json_url = drush_get_option('json_url', 'N/A');
  if ($json_url == "N/A") {
    print("\n Please provide the json_url value eg. --json_url=http://example.com/json \n\n");
    return;
  }
  $post_data = json_decode(file_get_contents($json_url));
  
  $i = 1;
  foreach($post_data as $item) {
    $importer = \Drupal::service('excercise_json_import.importer')->importPosts($item);
    if(!$importer) {
      $message = "\n$i. Title: ".$item->title." already exist.\n";
    } else {
      $message = "\n$i. Title: ".$item->title." created with Nid: ".$importer."\n";
    }
    print_r($message);
    $i++;
  }
}